PROJECT=vamp
tmux has-session -t $PROJECT
if [ $? != 0 ]
then
    tmux new-session -s $PROJECT -n editor -d
    tmux send-keys -t $PROJECT 'cd /Users/lparsons/Documents/workspace_4.2/vamp' C-m 
    tmux send-keys -t $PROJECT "workon ${PROJECT}" C-m
    tmux send-keys -t $PROJECT 'vim' C-m
    tmux split-window -v -p 25 -t $PROJECT
    #tmux select-layout -t $PROJECT main-horizontal
    tmux send-keys -t $PROJECT:1.2 'cd /Users/lparsons/Documents/workspace_4.2/vamp' C-m
    tmux send-keys -t $PROJECT "workon ${PROJECT}" C-m
    tmux new-window -n console -t $PROJECT
    tmux send-keys -t $PROJECT:2 'cd /Users/lparsons/Documents/workspace_4.2/vamp' C-m
    tmux send-keys -t $PROJECT "workon ${PROJECT}" C-m
    tmux select-window -t $PROJECT:1
    tmux select-pane -t $PROJECT:1.1
fi
tmux attach -t $PROJECT
