#!/bin/bash

LOG_PREFIX=""
if [[ ${HOSTNAME} == neak* ]]; then
    PROJECT="htseq"
    PROJECT_DIR="/Genomics/local/www/prod/htseq/"
    PROJECT_LOG_DIR="/Genomics/local/www/prod/logs/"
elif [[ ${HOSTNAME} == bixi* ]]; then
    DEFAULT_PROJECT="htseq-dev"
    read -e -p "Enter HTSEQ instance [${DEFAULT_PROJECT}]: " PROJECT
    PROJECT=${PROJECT:-${DEFAULT_PROJECT}}
    if [[ $PROJECT == "htseq-dev" ]]; then
        PROJECT_DIR="/Genomics/local/www/lparsons/htseq/"
        PROJECT_LOG_DIR="/Genomics/local/www/lparsons/logs/"
        LOG_PREFIX=""
    elif [[ $PROJECT == "htseq-stage" ]]; then
        PROJECT_DIR="/Genomics/local/www/stage/htseq/"
        PROJECT_LOG_DIR="/Genomics/local/www/stage/logs/"
        LOG_PREFIX=""
    fi
fi

echo ""
echo "HOSTNAME is ${HOSTNAME}"
echo "PROJECT is ${PROJECT}"
echo "PROJECT_DIR is ${PROJECT_DIR}"
echo "PROJECT_LOG_DIR is ${PROJECT_LOG_DIR}"
echo ""
if ! [ -d "${PROJECT_DIR}" ]; then
    echo "${PROJECT_DIR} not found" >&2
    echo ""
    exit 1
fi

cd "${PROJECT_DIR}"
tmux has-session -t $PROJECT
if [ $? != 0 ]
then
    tmux new-session -s ${PROJECT} -n editor -d
    tmux send-keys -t ${PROJECT} "cd \"${PROJECT_DIR}\"" C-m
    tmux send-keys -t ${PROJECT} 'vim' C-m
    tmux split-window -h -t ${PROJECT}
    tmux send-keys -t ${PROJECT}:1.2 "cd \"${PROJECT_DIR}\"" C-m

    tmux new-window -n logs -t $PROJECT
    tmux split-window -v -t ${PROJECT}:2
    tmux send-keys -t ${PROJECT}:2.1 "cd \"${PROJECT_LOG_DIR}\"" C-m
    tmux send-keys -t ${PROJECT}:2.1 "tail -F ${LOG_PREFIX}access_log.htseq" C-m
    tmux send-keys -t ${PROJECT}:2.2 "cd \"${PROJECT_LOG_DIR}\"" C-m
    tmux send-keys -t ${PROJECT}:2.2 "tail -F ${LOG_PREFIX}error_log.htseq" C-m
    tmux select-layout -t ${PROJECT}:2 even-vertical

    tmux select-window -t ${PROJECT}:1
    tmux select-pane -t ${PROJECT}:1.1
fi
tmux attach -t $PROJECT
