PROJECT=hmoller
PROJECT_DIR='/Users/lparsons/Documents/projects/hmoller/circular_yeast_dna'
tmux has-session -t $PROJECT
if [ $? != 0 ]
then
    tmux new-session -s $PROJECT -n editor -d
    tmux send-keys -t $PROJECT "cd ${PROJECT_DIR}" C-m 
    tmux send-keys -t $PROJECT "workon ${PROJECT}" C-m
    tmux send-keys -t $PROJECT 'vim' C-m
    tmux split-window -h -t $PROJECT
    #tmux select-layout -t $PROJECT main-horizontal
    tmux send-keys -t $PROJECT:1.2 "cd ${PROJECT_DIR}" C-m
    tmux send-keys -t $PROJECT "workon ${PROJECT}" C-m
    tmux new-window -n console -t $PROJECT
    tmux send-keys -t $PROJECT:2 "cd ${PROJECT_DIR}" C-m
    tmux send-keys -t $PROJECT "workon ${PROJECT}" C-m
    tmux new-window -n anaconda -t $PROJECT
    tmux split-window -h -t $PROJECT:3
    tmux send-keys -t $PROJECT:3.1 "cd ${PROJECT_DIR}" C-m
    tmux send-keys -t $PROJECT:3.1 "anaconda" C-m
    tmux send-keys -t $PROJECT:3.1 "vim" C-m
    tmux split-window -v -t $PROJECT:3.2
    tmux send-keys -t $PROJECT:3.2 "cd ${PROJECT_DIR}" C-m
    tmux send-keys -t $PROJECT:3.2 "anaconda" C-m
    tmux send-keys -t $PROJECT:3.2 "ipython --matplotlib osx" C-m
    tmux send-keys -t $PROJECT:3.3 "cd ${PROJECT_DIR}" C-m
    tmux send-keys -t $PROJECT:3.3 "anaconda" C-m
    tmux select-window -t $PROJECT:1
    tmux select-pane -t $PROJECT:1.1
fi
tmux attach -t $PROJECT
