PROJECT=tigerfish_latex
PROJECT_DIR=/Users/lparsons/Documents/projects/fish/manuscripts/tigerfish-manuscript
tmux has-session -t $PROJECT
if [ $? != 0 ]
then
    tmux new-session -s $PROJECT -n editor -d
    tmux send-keys -t $PROJECT "cd '${PROJECT_DIR}'" C-m
    tmux send-keys -t $PROJECT 'vim tigerfish.tex' C-m
    tmux split-window -v -p 25 -t $PROJECT
    tmux send-keys -t $PROJECT:1.2 "cd '${PROJECT_DIR}'" C-m
    tmux select-window -t $PROJECT:1
    tmux select-pane -t $PROJECT:1.1
fi
tmux attach -t $PROJECT
