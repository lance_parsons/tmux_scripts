#!/bin/bash

if [[ ${HOSTNAME} == lirone || ${HOSTNAME} == firmament ]]; then
    GALAXY_HOME="/Genomics/local/galaxy"
    DEFAULT_PROJECT="galaxy-prod"
else
    GALAXY_HOME="/galaxy"
    DEFAULT_PROJECT="galaxy-app"
fi

echo "HOSTNAME is ${HOSTNAME}"
echo "GALAXY_HOME is ${GALAXY_HOME}"
echo "DEFAULT_PROJECT is ${DEFAULT_PROJECT}"

if [ "$#" -lt 1 ]; then
    read -r -e -p "Enter the galaxy instance name [${DEFAULT_PROJECT}]: " PROJECT
    PROJECT=${PROJECT:-${DEFAULT_PROJECT}}
else
    PROJECT=${1%/}
fi
GALAXY_INSTANCE_DIR="${GALAXY_HOME}/${PROJECT}"
if ! [ -d "${GALAXY_INSTANCE_DIR}" ]; then
  echo "${PROJECT} not found in ${GALAXY_INSTANCE_DIR}" >&2
  echo "Usage: $0 [GALAXY_INSTANCE]" >&2
  exit 1
fi

echo ""
echo "Project: $PROJECT"
echo ""

if ! tmux has-session -t "${PROJECT}" ; then
    echo "Starting new session"
    tmux new-session -s "${PROJECT}" -n editor -d
    tmux send-keys -t "${PROJECT}" "cd \"${GALAXY_INSTANCE_DIR}\"" C-m 
    tmux send-keys -t "${PROJECT}" "vim" C-m
    tmux split-window -v -p 25 -t "${PROJECT}"
    tmux select-layout -t "${PROJECT}:1" even-horizontal
    tmux send-keys -t "${PROJECT}:1.2" "cd \"${GALAXY_INSTANCE_DIR}\"" C-m

    tmux new-window -n console -t "${PROJECT}"
    tmux send-keys -t "${PROJECT}:2" "cd \"$GALAXY_INSTANCE_DIR\"" C-m

    tmux new-window -n galaxy-logs -t "${PROJECT}"

    tmux send-keys -t "${PROJECT}:3" "cd \"${GALAXY_INSTANCE_DIR}\"" C-m
    tmux send-keys -t "${PROJECT}:3" "tail -f galaxy.log" C-m
 
    tmux new-window -n reports-logs -t "${PROJECT}"

    tmux send-keys -t "${PROJECT}:4" "cd \"${GALAXY_INSTANCE_DIR}\"" C-m
    tmux send-keys -t "${PROJECT}:4" "tail -f reports_webapp.log" C-m

    tmux select-window -t "${PROJECT}:1"
    tmux select-pane -t "${PROJECT}:1.1"
else
    echo "Attaching to existing session"
    tmux attach -t "${PROJECT}"
fi
