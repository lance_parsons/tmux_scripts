#!/bin/sh

DEFAULT_PROJECT="galaxy-localdev"
DEFAULT_VIRTUALENV="galaxy"


if [ "$#" -lt 1 ]; then
    read -e -p "Enter the galaxy instance name [${DEFAULT_PROJECT}]: " PROJECT
    PROJECT=${PROJECT:-${DEFAULT_PROJECT}}
else
    PROJECT=${1%/}
fi
GALAXY_INSTANCE_DIR="/Users/lparsons/Documents/projects/galaxy/${PROJECT}"
if ! [ -d "${GALAXY_INSTANCE_DIR}" ]; then
  echo "${PROJECT} not found" >&2
  echo "Usage: $0 [GALAXY_INSTANCE] [VIRTUALENV]" >&2
  exit 1
fi

if [ "$#" -lt 2 ]; then
    read -e -p "Enter the galaxy virtualenv name [${DEFAULT_VIRTUALENV}]: " VIRTUALENV
    VIRTUALENV=${VIRTUALENV:-${DEFAULT_VIRTUALENV}}
else
    VIRTUALENV=$2
fi
if ! [ -d "${WORKON_HOME}/${VIRTUALENV}" ]; then
    echo "${VIRTUALENV} virtualenv not found in WORKON_HOME (${WORKON_HOME})" >&2
  echo "Usage: $0 [GALAXY_INSTANCE] [VIRTUALENV]" >&2
  exit 1
fi

echo ""
echo "Project: $PROJECT"
echo "VirutalEnv: $VIRTUALENV"
echo ""

tmux has-session -t $PROJECT
if [ $? != 0 ]
then
    tmux new-session -s $PROJECT -n editor -d
    tmux send-keys -t $PROJECT "cd \"$GALAXY_INSTANCE_DIR\"" C-m 
    tmux send-keys -t $PROJECT "workon $VIRTUALENV" C-m
    tmux send-keys -t $PROJECT "vim" C-m
    tmux split-window -v -p 25 -t $PROJECT
    tmux send-keys -t $PROJECT:1.2 "cd \"$GALAXY_INSTANCE_DIR\"" C-m
    tmux send-keys -t $PROJECT "workon $VIRTUALENV" C-m

    tmux new-window -n console -t $PROJECT
    tmux send-keys -t $PROJECT:2 "cd \"$GALAXY_INSTANCE_DIR\"" C-m
    tmux send-keys -t $PROJECT:2 "workon $VIRTUALENV" C-m
    tmux send-keys -t $PROJECT:2 "sleep 30; open http://localhost:8080" C-m
    tmux send-keys -t $PROJECT:2 "open http://localhost:9001" C-m
    tmux send-keys -t $PROJECT:2 "open http://localhost:9009" C-m

    tmux new-window -n logs -t $PROJECT
    tmux split-window -v -t $PROJECT:3 
    tmux split-window -h -t $PROJECT:3.2

    tmux send-keys -t $PROJECT:3.1 "cd \"$GALAXY_INSTANCE_DIR\"" C-m
    tmux send-keys -t $PROJECT:3.1 "workon $VIRTUALENV" C-m
    tmux send-keys -t $PROJECT:3.1 "sh run.sh" C-m
 
    tmux send-keys -t $PROJECT:3.2 "cd \"$GALAXY_INSTANCE_DIR\"" C-m
    tmux send-keys -t $PROJECT:3.2 "workon $VIRTUALENV" C-m
    tmux send-keys -t $PROJECT:3.2 "sh run_reports.sh --sync-config &" C-m
    tmux send-keys -t $PROJECT:3.2 "tail -f reports_webapp.log" C-m

    tmux send-keys -t $PROJECT:3.3 "cd \"$GALAXY_INSTANCE_DIR\"" C-m
    tmux send-keys -t $PROJECT:3.3 "workon $VIRTUALENV" C-m
    tmux send-keys -t $PROJECT:3.3 "sh run_tool_shed.sh &" C-m
    tmux send-keys -t $PROJECT:3.3 "tail -f tool_shed_webapp.log" C-m

    tmux select-layout -t $PROJECT:3 even-vertical
    tmux select-window -t $PROJECT:1
    tmux select-pane -t $PROJECT:1.1
fi
tmux attach -t $PROJECT
