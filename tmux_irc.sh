PROJECT=irc
tmux has-session -t $PROJECT
if [ $? != 0 ]
then
    tmux new-session -s $PROJECT -d
    tmux send-keys -t $PROJECT 'weechat-curses' C-m 
fi
tmux attach -t $PROJECT
