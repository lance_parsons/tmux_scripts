PROJECT=tigerfish
MANUSCRIPT_DIR=/Users/lparsons/Documents/projects/fish/manuscripts/tigerfish-manuscript
WIKI_DIR=/Users/lparsons/Documents/projects/fish/tigerfish-wiki
EXAMPLES_DIR=/Users/lparsons/Documents/projects/fish/tigerfish-examples
CODE_DIR=/Users/lparsons/Documents/projects/fish/tigerfish
tmux has-session -t $PROJECT
if [ $? != 0 ]
then
    tmux new-session -s $PROJECT -n manuscript -d
    tmux send-keys -t $PROJECT "cd '${MANUSCRIPT_DIR}'" C-m
    tmux send-keys -t $PROJECT 'vim tigerfish.tex' C-m
    tmux split-window -v -p 25 -t $PROJECT
    tmux send-keys -t $PROJECT:1.2 "cd '${MANUSCRIPT_DIR}'" C-m

    tmux new-window -n wiki -t $PROJECT
    tmux send-keys -t $PROJECT "cd '${WIKI_DIR}'" C-m
    tmux send-keys -t $PROJECT 'vim Home.md' C-m
    tmux split-window -v -p 25 -t $PROJECT
    tmux send-keys -t $PROJECT:2.2 "cd '${WIKI_DIR}'" C-m
 
    tmux new-window -n examples -t $PROJECT
    tmux send-keys -t $PROJECT "cd '${EXAMPLES_DIR}'" C-m
    tmux send-keys -t $PROJECT 'workon fish' C-m
 
    tmux new-window -n matlab_code -t $PROJECT
    tmux send-keys -t $PROJECT "cd '${CODE_DIR}'" C-m
    tmux send-keys -t $PROJECT 'workon fish' C-m
    tmux send-keys -t $PROJECT 'vim' C-m
    
    tmux select-window -t $PROJECT:1
    tmux select-pane -t $PROJECT:1.1

fi
tmux attach -t $PROJECT
