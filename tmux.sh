#!/usr/bin/env bash

VIRTUALENV=0
CONDAENV=0
PROJECT=''

function usage {
    echo "Usage: $(basename "$0") [options] PROJECT_DIR"
    echo ""
    echo "    Options:"
    echo "        -v      Project has virtialenv"
    echo ""
}

while getopts ":v" opt; do
    case $opt in
        v) VIRTUALENV=1;;
        \?) echo "Invalid option: -$OPTARG" >&2
            usage
            exit 1
            ;;
    esac
done

# Set project dir and project name
shift "$((OPTIND-1))"
if [ $# -eq "0" ]; then
    PROJECT_DIR="${PWD}"
else
    PROJECT_DIR=$( cd -P "${1%/}" && pwd )
    #PROJECT_DIR=${1%/}
fi
PROJECT=${PROJECT_DIR##*/}

echo "PROJECT_DIR: ${PROJECT_DIR}"
echo "PROJECT: ${PROJECT}"

cd "${PROJECT_DIR}" || exit 2

# Periods and spaces not allowed in session names
PROJECT=${PROJECT//./_}
PROJECT=${PROJECT// /_}

# Detect conda env
#if "$([ -n "$_CONDA_EXE" ] && echo "$_CONDA_EXE" || which conda)" info >/dev/null 2>&1; then
echo "Detecting Conda"
if [ -e ${HOME}/miniconda3/etc/profile.d/conda.sh ]; then
    . ${HOME}/miniconda3/etc/profile.d/conda.sh
fi
type conda >/dev/null 2>&1
if [ $? -eq 0 ]; then
    echo "CONDA DETECTED"
    CONDAENV_LIST=$(conda env list --json | python -c 'import json,sys;obj=json.load(sys.stdin);print("\n".join(obj["envs"]))')
    for ENVPATH in ${CONDAENV_LIST}; do
        ENVNAME=${ENVPATH##*/}
        if [[ ${ENVNAME} =~ ${PROJECT} ]]; then
            CONDAENV=1
        fi
    done
else
    echo "CONDA NOT DETECTED"
fi

echo "Detecting virtualenv"
# Detect virtualenv
if [ -z "$DEST" ]; then
    if [ ! -z "$WORKON_HOME" ]; then
        VIRTUALENV_LIST=$(find "$WORKON_HOME" -maxdepth 1 -type d)
        if [[ $VIRTUALENV_LIST =~ $PROJECT ]]; then
            VIRTUALENV=1
        fi
    fi
fi

ENVCOMMAND=""
if [ $CONDAENV == 1 ]; then
    ENVCOMMAND="conda activate"
elif [ $VIRTUALENV == 1 ]; then
    ENVCOMMAND="workon"
fi


echo "Starting tmux"
if ! tmux has-session -t "$PROJECT"; then
    tmux new-session -s "$PROJECT" -n editor -d
    tmux split-window -h -t "$PROJECT:1"
    if [ ! -z "${ENVCOMMAND}" ]; then
        tmux send-keys -t "$PROJECT:1.1" "${ENVCOMMAND} ${PROJECT}" C-m
    fi
    tmux send-keys -t "$PROJECT:1.1" "vim" C-m
    if [ ! -z "${ENVCOMMAND}" ]; then
        tmux send-keys -t "$PROJECT:1.2" "${ENVCOMMAND} ${PROJECT}" C-m
    fi
    tmux new-window -n console -t "$PROJECT"
    if [ ! -z "${ENVCOMMAND}" ]; then
        tmux send-keys -t "$PROJECT:2" "${ENVCOMMAND} ${PROJECT}" C-m
    fi
    tmux select-window -t "$PROJECT:1"
    tmux select-pane -t "$PROJECT:1.1"
fi
tmux attach -t "$PROJECT"
